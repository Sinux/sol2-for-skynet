#include <lua.h>
#include <lauxlib.h>
#include <stdlib.h>

// 用户数据结构
typedef struct {
    int value;
} skynet_userdata;

// 设置成员变量的函数
static int set_value(lua_State *L) {
    skynet_userdata *ud = (skynet_userdata *) luaL_checkudata(L, 1, "skynet_userdata");
    int new_value = luaL_checkinteger(L, 2);
    ud->value = new_value;
    return 0;
}

// 获取成员变量的函数
static int get_value(lua_State *L) {
    skynet_userdata *ud = (skynet_userdata *) luaL_checkudata(L, 1, "skynet_userdata");
    lua_pushinteger(L, ud->value);
    return 1;
}

// 创建新的用户数据对象
static int new_userdata(lua_State *L) {
    skynet_userdata* ud = (skynet_userdata *)lua_newuserdata(L, sizeof(skynet_userdata));
    luaL_getmetatable(L, "skynet_userdata");
    lua_setmetatable(L, -2);

    ud->value = 0;  // 初始值
    return 1;
}

// 注册模块函数
static const struct luaL_Reg skynet_module[] = {
    {"new", new_userdata},
    {"set_value", set_value},
    {"get_value", get_value},
    {NULL, NULL}
};

// 注册用户数据的元表
static const struct luaL_Reg userdata_meta[] = {
    {"set_value", set_value},
    {"get_value", get_value},
    {NULL, NULL}
};

int luaopen_mymodule(lua_State *L) {
    luaL_newmetatable(L, "skynet_userdata");
    luaL_setfuncs(L, userdata_meta, 0);
    luaL_newlib(L, skynet_module);  // 创建库
    return 1;  // 返回库
}