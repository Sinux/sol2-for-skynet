local skynet = require "skynet"
require "skynet.manager"
local simplemodule = require "simplemodule"
local mymodule = require "mymodule"

skynet.start(function()
    print('load mymodule writing by c')
    print(mymodule)
    for key, value in pairs(mymodule) do
        print(key, value)
    end
    local inst = mymodule.new()
    mymodule.set_value(inst,1234)
    print('get val:', mymodule.get_value(inst))

    print('\nstart service writing by c')
    -- 启动 myservice
    local myservice = skynet.launch("myservice", "demo")
    skynet.send(myservice, "lua", "some_command")

    -- 打印服务地址
    skynet.error("myservice started:", skynet.address(myservice))

    print('\nload module writing by cpp')
    print(simplemodule)
    for key, value in pairs(simplemodule) do
        print(key, value)
    end

    local instance = simplemodule.SimpleModule.new(10)
    instance:greet()
    print("Value: ", instance:getValue())

    instance:setValue(20)
    print("New Value: ", instance:getValue())

    print('\nstart service writing by cpp')
    -- 启动 simpleservice
    local simpleservice = skynet.launch("simpleservice", "example")
    skynet.send(simpleservice, "lua", "some_command")

    -- 打印服务地址
    skynet.error("simpleservice started:", skynet.address(simpleservice))

end)

