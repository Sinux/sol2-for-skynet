cmake_minimum_required(VERSION 3.10)
project(SimpleModule)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_C_STANDARD 11)

include_directories(include)
include_directories(/root/skynet/3rd/lua)
include_directories(/root/skynet/skynet-src)

# module and service for cpp
add_library(simplemodule SHARED simplemodule.cpp)
add_library(simpleservice SHARED simpleservice.cpp)

# module and service for c
add_library(mymodule SHARED mymodule.c)
add_library(myservice SHARED myservice.c)
