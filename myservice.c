#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "skynet.h"
#include "skynet_handle.h"
#include "skynet_module.h"

// 数据结构
struct myservice {
    struct skynet_context *ctx;
};

// 消息处理函数
static int myservice_cb(struct skynet_context * ctx, void *ud, int type, int session, uint32_t source, const void * msg, size_t sz) {
    struct myservice *self = ud;

    if (type == PTYPE_RESERVED_LUA) {
        char *data = (char *) msg;
        skynet_error(ctx, "myservice received message from :%08x, session = %d, size = %zu, content = %s", source, session, sz, data);
    }

    return 0;
}

// 初始化函数
int myservice_init(void *inst, struct skynet_context *ctx, const char *args) {
    struct myservice *self = inst;
    self->ctx = ctx;
    skynet_error(ctx, "myservice initialized with args: %s", args);
    skynet_callback(ctx, self, myservice_cb);
    return 0;
}

// 释放函数
void myservice_release(void *inst) {
    struct myservice *self = inst;
    skynet_error(self->ctx, "myservice release");
    skynet_free(self);
}

struct myservice* myservice_create(void) {
    struct myservice *inst = skynet_malloc(sizeof(*inst));
    return inst;
}
