#include <sol/sol.hpp>
#include <iostream>

class SimpleModule {
public:
    SimpleModule(int value) : value(value) {}

    int getValue() const {
        return value;
    }

    void setValue(int newValue) {
        value = newValue;
    }

    void greet() const {
        std::cout << "Hello from C++! Current value is " << value << std::endl;
    }

private:
    int value;
};

// Module entry point
extern "C" int luaopen_simplemodule(lua_State* L) {
    std::cout << "load module successfully" << std::endl;
    sol::state_view lua(L);

    // Create a new table in Lua and bind the SimpleModule class
    sol::table module = lua.create_table();
    module.new_usertype<SimpleModule>("SimpleModule",
        sol::constructors<SimpleModule(int)>(),
        "getValue", &SimpleModule::getValue,
        "setValue", &SimpleModule::setValue,
        "greet", &SimpleModule::greet
    );

    return module.push();
}
