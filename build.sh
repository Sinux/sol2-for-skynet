#!/usr/bin/env bash

cmake -S . -B build && cmake --build build && \
mv -f build/libsimpleservice.so ../skynet/cservice/simpleservice.so && \
mv -f build/libsimplemodule.so ../skynet/luaclib/simplemodule.so && \
mv -f build/libmyservice.so ../skynet/cservice/myservice.so && \
mv -f build/libmymodule.so ../skynet/luaclib/mymodule.so && \
cp -f lua/myconfig ../skynet/examples/myconfig && \
cp -f lua/mymodule.lua ../skynet/examples/mymodule.lua \
